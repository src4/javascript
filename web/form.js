var x = document.getElementById("form_sample");
var createform = document.createElement('form');
createform.setAttribute("action", ""); // Setting Action Attribute on Form
createform.setAttribute("method", "get");
x.appendChild(createform);



var heading = document.createElement('h2');
heading.innerHTML = "Login here";
createform.appendChild(heading);

var line = document.createElement('hr');
createform.appendChild(line);

var linebreak = document.createElement('br');
createform.appendChild(linebreak);

var email = document.createElement('label');
email.innerHTML = "Email:";
createform.appendChild(email);


var emailele = document.createElement('input');
emailele.style.outline = 'none'
emailele.setAttribute("type", "email");
//emailele.setAttribute("name", "em");
createform.appendChild(emailele);
var ebreak = document.createElement('br');
createform.appendChild(ebreak);
emailele.oninput = (e) => {
    emailcheck(e.target)
};

var buttonelement = document.createElement('input');
buttonelement.setAttribute("type", "submit");
buttonelement.setAttribute("name", "btn");
createform.appendChild(buttonelement);
buttonelement.setAttribute("value", "Go");
buttonelement.style.outline = 'none';


const setButtonState = (status) => {
    if (status) {
        buttonelement.style.pointerEvents = 'auto'
        buttonelement.style.opacity = 1;

    } else {
        buttonelement.style.pointerEvents = 'none'
        buttonelement.style.opacity = 0.5;
    }
}


function emailcheck(emailele) {
    //alert("working");
    var res = emailele.value;
    var str = res.replace(/.*@/, "");
    if (str.toLowerCase() === "consultadd.com") {
        emailele.style.borderColor = 'green';
        emailele.style.borderRadius = '12px';
        //alert("Your email is correct");
        para.innerText = "Your email is correct";
        setButtonState(true)
            //buttonelement.disabled = true;
        async function fetchData() {
            const URL = "https://demo7857661.mockable.io/testdata";
            const fetchResult = fetch(URL)
            const response = await fetchResult;
            const jsonData = await response.json();
            localStorage.setItem('Name', jsonData.firstName);
            sessionStorage.setItem('Age', jsonData.age);
            //sessionStorage.setItem('Age :', jsonData.age);
            //console.log('Age', jsonData[0].age);
        }

        fetchData('javascript');

    } else {
        emailele.style.borderColor = 'red';
        emailele.style.borderRadius = '12px';
        //alert("your email is invaild");
        para.innerText = "Your email is invalid";
        //buttonelement.disabled = true;
        setButtonState(false)

    }
}
var para = document.createElement('p');
para.appendChild(document.createTextNode(""));
createform.appendChild(para);

/*fetch("https://demo7857661.mockable.io/testdata")
    .then(res => res.json())
    .then(json => {
        localStorage.setItem('name', json.firstName)
    }, console.log('Age',
        json.age));*/


createform.onsubmit = function(a) {
    a.preventDefault();
    window.location.assign("new.html");
}


//buttonelement.onclick = function() { emailcheck(emailele) };
var submitelement = document.createElement('input');
submitelement.setAttribute("type", "reset");
submitelement.setAttribute("name", "dsubmit");
createform.appendChild(submitelement);
submitelement.style.outline = 'none';
submitelement.setAttribute("value", "Clear");